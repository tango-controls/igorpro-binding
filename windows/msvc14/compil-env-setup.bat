@ECHO off

:: ======== FULL TANGO RUNTIME PATHS ======== 

::- git clone directory
set BINDING_PATH=C:\Projects\igorpro-binding@gitlab

::- runtime directories
set RUNTIME_PATH_X86=C:\Projects\tango-10.0.0-rc1-v143-x86
set RUNTIME_PATH_X64=C:\Projects\tango-10.0.0-rc1-v143-x64

::- .h
set RUNTIME_INC_X64=%RUNTIME_PATH_X64%\include
set RUNTIME_INC_X86=%RUNTIME_PATH_X86%\include

::- .lib
set RUNTIME_LIB_X64=%RUNTIME_PATH_X64%\lib
set RUNTIME_LIB_X86=%RUNTIME_PATH_X86%\lib

::- .dll
set RUNTIME_BIN_X64=%RUNTIME_PATH_X64%\bin
set RUNTIME_BIN_X86=%RUNTIME_PATH_X86%\bin

:: ======== PATH ======== 
set PATH=%RUNTIME_BIN_X64%;%PATH%
set PATH=%RUNTIME_BIN_X86%;%PATH%

:: ======== OMNIORB ======== 

SET OMNIORB_INC_X86=%RUNTIME_INC_X86%
SET OMNIORB_LIB_X86=%RUNTIME_LIB_X86%
SET OMNIORB_LIB_LIST_X86=omnithread_rt.lib;omniORB4_rt.lib;COS4_rt.lib;omniDynamic4_rt.lib
SET OMNIORB_LIB_DEBUG_LIST_X86=omnithread_rt.lib;omniORB4_rt.lib;COS4_rt.lib;omniDynamic4_rt.lib

SET OMNIORB_INC_X64=%RUNTIME_INC_X64%
SET OMNIORB_LIB_X64=%RUNTIME_LIB_X64%
SET OMNIORB_LIB_LIST_X64=omnithread_rt.lib;omniORB4_rt.lib;COS4_rt.lib;omniDynamic4_rt.lib
SET OMNIORB_LIB_DEBUG_LIST_X64=omnithread_rt.lib;omniORB4_rt.lib;COS4_rt.lib;omniDynamic4_rt.lib

:: ======== TANGO ======== 

SET TANGO_INC_X86=%RUNTIME_INC_X86%\tango
SET TANGO_LIB_X86=%RUNTIME_LIB_X86%
SET TANGO_LIB_LIST_X86=tango.lib
SET TANGO_LIB_DEBUG_LIST_X86=turbojpeg.lib;tangod.lib

SET TANGO_INC_X64=%RUNTIME_INC_X64%\tango
SET TANGO_LIB_X64=%RUNTIME_LIB_X64%
SET TANGO_LIB_LIST_X64=tango.lib
SET TANGO_LIB_DEBUG_LIST_X64=turbojpeg.lib;tangod.lib

:: ======== IGOR PRO XOP ======== 

set XOP_TOOLKIT_ROOT=C:\Projects\igorpro-binding@gitlab\xop-toolkit-7.1

set XOP_INC_X86=%XOP_TOOLKIT_ROOT%\include
set XOP_RC_X86=%XOP_TOOLKIT_ROOT%\resource
set XOP_LIB_X86=%XOP_TOOLKIT_ROOT%\lib\x86
set XOP_LIB_LIST_X86=Igor.lib;XOPSupport32.lib

set XOP_INC_X64=%XOP_TOOLKIT_ROOT%\include
set XOP_RC_X64=%XOP_TOOLKIT_ROOT%\resource
set XOP_LIB_X64=%XOP_TOOLKIT_ROOT%\lib\x64
set XOP_LIB_LIST_X64=Igor64.lib;XOPSupport64.lib
